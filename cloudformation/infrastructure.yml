AWSTemplateFormatVersion: "2010-09-09"
Description: Cobblestone DevOps Test CloudFormation Template

Parameters:
  EnableAdminAccess:
    Type: String
    AllowedValues: [ "true", "false" ]
    Description: Enable / Disable Admin Access
    Default: true

Conditions:
  IsAdminAccessEnabled: !Equals
    - !Ref EnableAdminAccess
    - true

Resources:
  # VPC AND SUBNETS
  CobblestoneVPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 172.31.0.0/16
      EnableDnsSupport: 'true'
      EnableDnsHostnames: 'true'

  InternetGateway:
    Type: AWS::EC2::InternetGateway

  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref CobblestoneVPC

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref CobblestoneVPC

  PublicRoute: # Public route table has direct routing to IGW:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  CobblestonePublicSubnet1A:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref CobblestoneVPC
      MapPublicIpOnLaunch: true
      CidrBlock: 172.31.15.0/20
      AvailabilityZone: ap-south-1a

  CobblestonePublicSubnet1B:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref CobblestoneVPC
      MapPublicIpOnLaunch: true
      CidrBlock: 172.31.31.0/20
      AvailabilityZone: ap-south-1b

  CobblestonePublicSubnet1C:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref CobblestoneVPC
      MapPublicIpOnLaunch: true
      CidrBlock: 172.31.47.0/20
      AvailabilityZone: ap-south-1c

  AssociateRouteTableAndPublicSubnet1A:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CobblestonePublicSubnet1A

  AssociateRouteTableAndPublicSubnet1B:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CobblestonePublicSubnet1B

  AssociateRouteTableAndPublicSubnet1C:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref CobblestonePublicSubnet1C

  # S3 BUCKET FOR STORING ARTIFACTS
  ArtifactBucket:
    Type: AWS::S3::Bucket

  # ECR TO STORE DOCKER IMAGES
  EcrRepository:
    Type: AWS::ECR::Repository
    Properties:
      RepositoryPolicyText:
        Version: "2012-10-17"
        Statement:
          - Sid: AllowPushPull
            Effect: Allow
            Principal:
              AWS:
                - "arn:aws:iam::112276299784:user/Cobblestone"
            Action:
              - "ecr:GetDownloadUrlForLayer"
              - "ecr:BatchGetImage"
              - "ecr:BatchCheckLayerAvailability"
              - "ecr:PutImage"
              - "ecr:InitiateLayerUpload"
              - "ecr:UploadLayerPart"
              - "ecr:CompleteLayerUpload"

  # SECURITY GROUP RESOURCES
  ElbSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for ELB
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: 0.0.0.0/0
      VpcId: !Ref CobblestoneVPC

  DBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Securtiy group for DB
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: 0.0.0.0/0
      VpcId: !Ref CobblestoneVPC

  EC2AutoScalingSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Securtiy group for EC2 Auto Scaling
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: 0.0.0.0/0
      VpcId: !Ref CobblestoneVPC

  EksClusterSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Securtiy group for EKS Cluster
      SecurityGroupEgress:
        - IpProtocol: -1
          CidrIp: 0.0.0.0/0
      VpcId: !Ref CobblestoneVPC

  # SECURITY GROUP INGRESS RULES
  SecurityGroupELBIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref ElbSecurityGroup
      IpProtocol: tcp
      FromPort: 80
      ToPort: 80
      CidrIp: 0.0.0.0/0

  SecurityGroupELBToEC2AutoScalingIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref EC2AutoScalingSecurityGroup
      IpProtocol: tcp
      FromPort: 5000
      ToPort: 5000
      SourceSecurityGroupId: !GetAtt ElbSecurityGroup.GroupId

  SecurityGroupDBFromEC2AutoScalingIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref DBSecurityGroup
      IpProtocol: tcp
      FromPort: 5432
      ToPort: 5432
      SourceSecurityGroupId: !GetAtt EC2AutoScalingSecurityGroup.GroupId

  SecurityGroupDBFromEKSIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref DBSecurityGroup
      IpProtocol: tcp
      FromPort: 5432
      ToPort: 5432
      SourceSecurityGroupId: !GetAtt EksClusterSecurityGroup.GroupId

  SecurityGroupDBAdminAccessIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Condition: IsAdminAccessEnabled
    Properties:
      GroupId: !Ref DBSecurityGroup
      IpProtocol: tcp
      FromPort: 5432
      ToPort: 5432
      CidrIp: 0.0.0.0/0

  SecurityGroupEC2AdminAccessIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Condition: IsAdminAccessEnabled
    Properties:
      GroupId: !Ref EC2AutoScalingSecurityGroup
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      CidrIp: 0.0.0.0/0

  # DATABASE RESOURCES
  PostgressDatabase:
    Type: AWS::RDS::DBInstance
    DeletionPolicy: Delete
    Properties:
      DBInstanceClass: db.t3.micro
      DBSubnetGroupName: !Ref PostgressSubnetGroup
      AllocatedStorage: 20
      Engine: postgres
      EngineVersion: 13.4
      MasterUsername: dbuser
      MasterUserPassword: dbpassword
      PubliclyAccessible: true
      VPCSecurityGroups:
        - !Ref DBSecurityGroup

  PostgressSubnetGroup:
    Type: AWS::RDS::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: DB Subnet Group for Postgress
      SubnetIds:
        - !Ref CobblestonePublicSubnet1A
        - !Ref CobblestonePublicSubnet1B
        - !Ref CobblestonePublicSubnet1C

  # APPLICATION RESOURCES
  CodeDeployLoadBalancer:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      Scheme: internet-facing
      SecurityGroups:
        - !Ref ElbSecurityGroup
      Subnets:
        - !Ref CobblestonePublicSubnet1A
        - !Ref CobblestonePublicSubnet1B
        - !Ref CobblestonePublicSubnet1C
      CrossZone: true
      Listeners:
        - InstancePort: '5000'
          InstanceProtocol: HTTP
          LoadBalancerPort: '80'
          Protocol: HTTP
      HealthCheck:
        Target: HTTP:5000/api/v1/
        HealthyThreshold: '2'
        UnhealthyThreshold: '3'
        Interval: '60'
        Timeout: '5'

  CodeDeployApplication:
    Type: AWS::CodeDeploy::Application
    Properties:
      ComputePlatform: Server

  CodeDeployDeploymentGroup:
    Type: AWS::CodeDeploy::DeploymentGroup
    Properties:
      ApplicationName: !Ref CodeDeployApplication
      DeploymentConfigName: CodeDeployDefault.OneAtATime
      DeploymentGroupName: recipes-app-deployment-group
      DeploymentStyle:
        DeploymentOption: WITH_TRAFFIC_CONTROL
      ServiceRoleArn: !GetAtt CodeDeployServiceRole.Arn
      AutoScalingGroups:
        - !Ref CodeDeployAutoScalingGroup
      LoadBalancerInfo:
        ElbInfoList:
          - Name: !Ref CodeDeployLoadBalancer

  CodeDeployAutoScalingGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      VPCZoneIdentifier:
        - !Ref CobblestonePublicSubnet1A
        - !Ref CobblestonePublicSubnet1B
        - !Ref CobblestonePublicSubnet1C
      LaunchConfigurationName: !Ref CodeDeployLaunchConfiguration
      MinSize: 1
      MaxSize: 4
      DesiredCapacity: 2

  CodeDeployLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      ImageId: ami-052cef05d01020f1d # valid for ap-south-1
      SecurityGroups:
        - !Ref EC2AutoScalingSecurityGroup
      InstanceType: t2.small
      IamInstanceProfile: !Ref EC2InstanceProfile
      UserData:
        Fn::Base64:
          !Sub |
          #!/bin/bash -xe

          sudo yum -y update
          sudo yum -y install ruby wget python3-pip awscli
          cd /home/ec2-user
          wget https://aws-codedeploy-ap-south-1.s3.ap-south-1.amazonaws.com/latest/install
          sudo chmod +x ./install
          sudo ./install auto

  CodeDeployServiceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - codedeploy.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole

  EC2InstanceProfile:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Path: /
      Roles:
        - !Ref EC2InstanceRole

  EC2InstanceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: /
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: Allow
                Action: '*'
                Resource: '*'
              - Effect: Allow
                Action:
                  - ec2:RunInstances
                  - ec2:CreateTags
                  - iam:PassRole
                  - elasticloadbalancing:Describe*
                  - elasticloadbalancing:DeregisterInstancesFromLoadBalancer
                  - elasticloadbalancing:RegisterInstancesWithLoadBalancer
                  - autoscaling:Describe*
                  - autoscaling:EnterStandby
                  - autoscaling:ExitStandby
                  - autoscaling:UpdateAutoScalingGroup
                  - autoscaling:SuspendProcesses
                  - autoscaling:ResumeProcesses
                Resource: '*'

  # EKS CLUSTER RESOURCES
  EksCluster:
    Type: AWS::EKS::Cluster
    Properties:
      Version: "1.21"
      RoleArn: !GetAtt EksClusterRole.Arn
      ResourcesVpcConfig:
        SecurityGroupIds:
          - !Ref EksClusterSecurityGroup
        SubnetIds:
          - !Ref CobblestonePublicSubnet1A
          - !Ref CobblestonePublicSubnet1B
          - !Ref CobblestonePublicSubnet1C

  EksNodegroup:
    Type: AWS::EKS::Nodegroup
    Properties:
      ClusterName: !Ref EksCluster
      NodeRole: !GetAtt EksNodeInstanceRole.Arn
      ScalingConfig:
        MinSize: 1
        DesiredSize: 1
        MaxSize: 3
      Subnets:
        - !Ref CobblestonePublicSubnet1A
        - !Ref CobblestonePublicSubnet1B
        - !Ref CobblestonePublicSubnet1C

  EksClusterRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - eks.amazonaws.com
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEKSClusterPolicy
        - arn:aws:iam::aws:policy/AmazonEKSServicePolicy

  EksNodeInstanceRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
        - "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
        - "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      Path: /

Outputs:
  EcrRepositoryName:
    Description: Value for ECR_REPOSITORY_NAME
    Value: !Ref EcrRepository
  ArtifactBucketName:
    Description: Value for ARTIFACT_S3_BUCKET
    Value: !Ref ArtifactBucket
  CodeDeployApplicationName:
    Description: Value for CODEDEPLOY_APPLICATION_NAME
    Value: !Ref CodeDeployApplication
  DBEndpoint:
    Description: Value for DB_ADDRESS
    Value: !GetAtt PostgressDatabase.Endpoint.Address
  EksClusterName:
    Description: Value for EKS_CLUSTER_NAME
    Value: !Ref EksCluster
  CodeDeployELB:
    Description: CodeDeploy ELB
    Value: !GetAtt CodeDeployLoadBalancer.DNSName