FROM python:3.6

# Install dependencies
COPY . /app
RUN python3 -m pip install --upgrade pip \
    && python3 -m pip install -r /app/requirements.txt

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app
CMD ["python", "manage.py", "runserver", "--host", "0.0.0.0", "--threaded"]