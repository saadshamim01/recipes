#!/usr/bin/env bash

echo 'Running EC2 Start script'
sudo docker run -id \
  -e SECRET_KEY='<SECRET_VALUE>' \
  -e FLASK_CONFIG='production' \
  -e DATABASE_URL='postgresql://<DB_USERNAME>:<DB_PASSWORD>@<DB_ADDRESS>:5432/recipe_db' \
  -p 5000:5000 \
  <AWS_ACCOUNT_ID>.dkr.ecr.ap-south-1.amazonaws.com/<ECR_REPOSITORY_NAME>:<IMAGE_TAG>
