#!/usr/bin/env bash

echo 'Running EC2 Stop script'
# Stop all docker containers
sudo docker rm -f $(sudo docker ps -a -q)
