#!/usr/bin/env bash

sudo yum -y update
sudo amazon-linux-extras install docker
sudo systemctl start docker

aws ecr get-login-password --region ap-south-1 | sudo docker login --username AWS --password-stdin <AWS_ACCOUNT_ID>.dkr.ecr.ap-south-1.amazonaws.com
